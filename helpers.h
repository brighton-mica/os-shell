#ifndef TASH_HELPERS_H
#define TASH_HELPERS_H

#include <stdlib.h>   // NULL
#include <ctype.h>    // isspace
#include <string.h>   // strdup
#include <unistd.h>   // write, STD*_FILENO
#include <stdbool.h>  // bool
#include <stdio.h>    // printf, perror
#include <assert.h>   // assert

// #define DEBUG

// tash accepts four types of commands. The Command_Type enum is used to in tandum with
// "get_cmd_type()" to route a Command structure to its correct processing function stored
// in "cmd_funcs[]".
enum Command_Type
{
    eEXIT      = 0,
    eCD        = 1,
    ePATH      = 2,
    eOTHER     = 3,
    eCMDSIZE   = 4
};

// The Command structure is the bread/butter of tash's parsing and processing. After
// the parsing pass, a Command will hold all needed information to process a command with
// ease.
// A Command structure encapsulates a "command" to process. A "command" in tash is simply
// input which launches a process.
//      "ls"-> 1 command
//      "ls & mkdir test" -> 2 commands
struct Command
{
    char* raw_input;   // "A B C"
    char** args;       // { "A", "B", "C" }
    size_t num_args;   // 3
};

// this is ugly, but we needed to find a quick way to hide error_msg() calls that should print out
// for the parent process (shell) but not for child. there are a lot of better ways to do this...
bool g_is_child_process = false;

void error_msg();
void free_command_list(const size_t num_commands, struct Command** command_list);
char* trim_whitespace(char* str);
bool multiple_parallel_ops(const char* str);
bool is_all_space(const char* str);
size_t num_occurrences_single_char(const char* str, const char c);
size_t get_arg_count(char* input_str, const char* delimiter);
void get_args(char* input_str, const size_t num_args, char*** args, const char* delimiter);
void get_commands(char* input_str, const size_t num_commands, struct Command** command_list, const char* delimiter);
bool parse(char* input, size_t* num_commands, struct Command** command_list, const char* command_delimiter);
int get_cmd_type(char* const Command_Type);

#ifdef DEBUG
void print_arr( char** const arr, const size_t size);
void print_cmds(struct Command* command_list, const size_t size);
#endif

// This file used to be split up into a .h and a .c, but in order to compile our program with the command
// given in the instructions, we threw it all in this .h (UGLY)

void error_msg()
{
     // my local limux system complains that the return value from write is not checked, but this is 
     // code he gave us... given that, I'm going to keep it as it is.
     char error_message[30] = "An error has occurred\n"; 
     write(STDERR_FILENO, error_message, strlen(error_message));
}

// this function takes care of the memory freeing for the entire list of commands. this should
// be split into two functions (one args free and another commands free (which would call args
// free))
void free_command_list(const size_t num_commands, struct Command** command_list)
{
    for (size_t cmd_idx = 0; cmd_idx < num_commands; ++cmd_idx)
    {
        free((*command_list)[cmd_idx].raw_input);
        (*command_list)[cmd_idx].raw_input = NULL;

        for (size_t arg_idx = 0; arg_idx < (*command_list)[cmd_idx].num_args + 1; ++arg_idx)
        {
            free((*command_list)[cmd_idx].args[arg_idx]);
            (*command_list)[cmd_idx].args[arg_idx] = NULL;
        }

        free((*command_list)[cmd_idx].args);
        (*command_list)[cmd_idx].args = NULL;
    }
    free(*command_list);
    *command_list = NULL;
}

// Trims all leading and trailing whitespace characters. mallocs
// space for new string.
char* trim_whitespace(char* str)
{
    if (str == NULL)
    {
        return NULL;
    }

    char* start_ptr = str;

    if (start_ptr == NULL) 
    {
        error_msg();
        return NULL;
    }
    
    for (; isspace(*start_ptr); start_ptr++);

    for (size_t i = strlen(start_ptr) - 1; isspace(start_ptr[i]); i--)
    {
        start_ptr[i] = '\0';
    }

    return start_ptr;
}

// searches for an occurence of "&&" within a string
bool multiple_parallel_ops(const char* str)
{
    if (str == NULL)
    {
        return false;
    }
    const size_t size = strlen(str);
    if (size < 2)
    {
        return false;
    }

    size_t i = 1;
    size_t j = 0;
    while (str[i] != '\0')
    {
        if (str[i] == '&' && str[j] == '&')
        {
            return true;
        }
        i++; 
        j++;
    }

    return false;
}

// returns true if the string is all whitespace, false otherwise
bool is_all_space(const char* str)
{
    if (str == NULL) 
    {
        return false;
    }
    size_t i = 0;
    while (str[i] != '\0')
    {
        if (!isspace(str[i]))
        {
            return false;
        }
        i++;
    }
    return true;
}

// the function name says it all
size_t num_occurrences_single_char(const char* str, const char c)
{
    size_t count = 0;
    for (size_t i = 0; str[i] != '\0'; i++)
    {
        if (str[i] == c)
            count++;
    }
    return count;
}

// get_arg_count() - gets the # of args/tokens in a string (specified by delimiter). also does err checking
// for commands that are all whitespace. for example...
// "ls &    & pwd" -> { "ls", "   ", "pwd" }. the middle command is bogus. if there is a bogus command,
// this funciton will return 0
size_t get_arg_count(char* input_str, const char* delimiter)
{
    size_t num_args = 0;
    bool command_thats_whitepace_only = false;

    char* tmp = strdup(input_str); 
    char* token = strtok(tmp, delimiter); 
    while (token != NULL)
    {
        if (is_all_space(token))
        {
            command_thats_whitepace_only = true;
            break;
        }

        num_args++;
        token = strtok(NULL, delimiter);
    }
    free(tmp);

    if (command_thats_whitepace_only)
    {
        num_args = 0;
    }

    return num_args;
}

// this function gets raw char* arguments (denoted by passed in delimiter)
void get_args(char* input_str, const size_t num_args, char*** args, const char* delimiter)
{
    // execv requires a NULL terminated sequence of arguements. Add one to the # of args here
    // and set it to NULL to satisfy execv requirements.
    *args = (char**)malloc((num_args + 1) * sizeof(char*));

    size_t i = 0;
    char* tmp = strdup(input_str);
    char* token = strtok(tmp, delimiter);
    while (token != NULL)
    {
        const size_t n_bytes = (strlen(token) + 1) * sizeof(char);
        (*args)[i] = malloc(n_bytes);
        strcpy((*args)[i], token);

        token = strtok(NULL, delimiter);
        i++;
    }

    (*args)[num_args] = NULL;
    free(tmp);
}

//  this function takes care of getting the commands from the input, and puts all the commands into a list of commands.
//  a Command is the first level of "parsing" that we do. We split commands by "&". Following, each command is parsed for
//  individual arguments. see Command strucutre for example.
void get_commands(char* input_str, size_t num_commands, struct Command** command_list, const char* delimiter)
{
    *command_list = (struct Command*)malloc(sizeof(struct Command)*(num_commands));

    size_t i = 0;
    char* tmp = strdup(input_str);
    char* token = strtok(tmp, delimiter);
    while (token != NULL)
    {
        const size_t n_bytes = (strlen(token) + 1) * sizeof(char);
        (*command_list)[i].raw_input = malloc(n_bytes);
        strcpy((*command_list)[i].raw_input, token);
        token = strtok(NULL, delimiter);
        i++;
    }
    free(tmp);
}

// parse - main entry point for processing user input. the main steps are as follows
// 1. trim pass
// 2. && check pass
// 3. arg count pass
// 4. command pass
// 5. command args pass
// see Command struture (this function just populates a list of these structures)
bool parse(char* input, size_t* num_commands, struct Command** command_list, const char* command_delimiter) 
{
    // trim all leading/trailing whitespace
    if (input == NULL) return false;
    char* trimmed_input = trim_whitespace(input);
    if (trimmed_input == NULL || *trimmed_input == '\0') return false;

    // handles case of multiple "&&". without this check, "ls &&& pwd" would be treated the same as
    // "ls & pwd". i've talked about this a couple other places, but we need to handle these "special"
    // cases better. i'm not a fan of putting if checks everywhere to suit the needs of one "special", 
    // and rather uncommon case. TODO
    if (multiple_parallel_ops(input))
    {
        if (!g_is_child_process)
        {
            error_msg();
        }
        return false;
    }

    // get # of valid commands. a command is not valid if it is all space. we handle valid commands here
    // so that we don't have to do further processing on bogus input.
    const size_t num_cmds = get_arg_count(trimmed_input, command_delimiter);

    // only n# of `command_delimiters` entered
    if (num_cmds == 0)
    {
        if (!g_is_child_process)
        {
            error_msg();
        }
        return false;
    }

    get_commands(trimmed_input, num_cmds, command_list, command_delimiter);
    struct Command* cmd_list = *command_list;

    if (cmd_list == NULL)
    {
        return false;
    }

    //  splice input string and store individial args
    for (size_t i = 0; i < num_cmds; ++i)
    {
        cmd_list[i].num_args = get_arg_count(cmd_list[i].raw_input, " \n\t");
        get_args(cmd_list[i].raw_input, cmd_list[i].num_args, &(cmd_list[i].args), " \n\t");
    }

    *num_commands = num_cmds;
    return true;
}

// this function checks for which command is supposed to be executed.
int get_cmd_type(char* const Command_Type)
{
    // exit command
    if (strcmp(Command_Type, "exit") == 0 )
    {
        return eEXIT;
    }
    // cd command
    if (strcmp(Command_Type, "cd") == 0 )
    {
        return eCD;
    }
    // path command
    if (strcmp(Command_Type, "path") == 0 )
    {
        return ePATH;
    }
    // rest of the other commands.
    return eOTHER;
}

#ifdef DEBUG
// used for debugging purposes only.

// prints the array 
void print_arr( char** const arr, const size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        printf(" `%s` ", arr[i]);
    }
    printf("\n");
}

// prints the list of commands
void print_cmds(struct Command* command_list, const size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        printf("Command %lu\n", i);
        printf("\tnum_args: %lu\n", command_list[i].num_args); 
        printf("\traw_input: %s\n\t", command_list[i].raw_input);
        print_arr(command_list[i].args, command_list[i].num_args);
        printf("\n");
    }
    printf("\n");
}
#endif

#endif // TASH_HELPERS_H
